//
//  AppDelegate.h
//  BreakoutGame
//
//  Created by Chirag Khubchandani on 3/28/17.
//  Copyright © 2017 Chirag Khubchandani. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

