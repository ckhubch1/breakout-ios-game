//
//  SecondViewController.h
//  BreakoutGame
//
//  Created by Chirag Khubchandani on 3/30/17.
//  Copyright © 2017 Chirag Khubchandani. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AudioToolbox/AudioToolbox.h>

@interface SecondViewController : UIViewController
{
    SystemSoundID sound;
    float dx, dy;
}

@property (strong, nonatomic) IBOutlet UIButton *Restart;
@property (nonatomic, strong) UIView *paddle;
@property (nonatomic, strong) UIView *ball;
@property (nonatomic, strong) NSTimer *timer;
@property(nonatomic,strong) UILabel *brick,*brick2,*brick3,*brick4,*brick5,*brick6,*brick7;
@property(nonatomic,strong) NSMutableArray *bricks;
@property (strong, nonatomic) UILabel *WelcomeMsg;

@end
