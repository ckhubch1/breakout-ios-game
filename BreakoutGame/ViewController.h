//
//  ViewController.h
//  BreakoutGame
//
//  Created by Chirag Khubchandani on 3/28/17.
//  Copyright © 2017 Chirag Khubchandani. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AudioToolbox/AudioToolbox.h>

@interface ViewController : UIViewController
{
    
SystemSoundID sound;
}

@property (strong, nonatomic) IBOutlet UILabel *brick;
@property (nonatomic, strong) NSTimer *timer;
@property (strong, nonatomic) IBOutlet UIButton *PlayButton;
@property (strong, nonatomic) UILabel *nameB;
@property (strong, nonatomic) UILabel *nameR;
@property (strong, nonatomic) UILabel *nameE;
@property (strong, nonatomic) UILabel *nameA;
@property (strong, nonatomic) UILabel *nameK;
@property (strong, nonatomic) UILabel *nameO;
//SystemSoundID sound;

@end

